package schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import schedule.models.*;
import schedule.service.ScheduleService;

import java.util.List;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private ScheduleService scheduleService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... arg0) throws Exception {

        for (Room room : scheduleService.getRooms()) {
            System.out.println("Room : " + room.getRoomNumber() + " - " + room.getId());
        }

        List<Module> modules = scheduleService.getModules();
        List<Professor> professors = scheduleService.getProfessors();
        List<Room> rooms = scheduleService.getRooms();
        List<Group> groups = scheduleService.getGroups();
        List<Timeslot> timeslots = scheduleService.getTimeslots();

        Timetable timetable = new Timetable(rooms, professors, modules, groups, timeslots);

        generateClassrooms(timetable);
    }

    public Classroom2[] generateClassrooms(Timetable timetable) {
        // Initialize GA
        GeneticAlgorithm ga = new GeneticAlgorithm(100, 0.01, 0.9, 2, 5);

        // Initialize population
        Population population = ga.initPopulation(timetable);

        // Evaluate population
        ga.evalPopulation(population, timetable);

        // Keep track of current generation
        int generation = 1;

        // Start evolution loop
        while (ga.isTerminationConditionMet(generation, 1000) == false && ga.isTerminationConditionMet(population) == false) {
            // Print fitness
            System.out.println("G" + generation + " Best fitness: " +
                    population.getFittest(0).getFitness());
            // Apply crossover
            population = ga.crossoverPopulation(population);
            // Apply mutation
            population = ga.mutatePopulation(population, timetable);
            // Evaluate population
            ga.evalPopulation(population, timetable);
            // Increment the current generation
            generation++;
        }

        // Print fitness
        timetable.createClasses(population.getFittest(0));
        System.out.println();
        System.out.println("Solution found in " + generation + " generations");
        System.out.println("Final solution fitness: " + population.getFittest(0).getFitness());
        System.out.println("Clashes: " + timetable.calcClashes());
        // Print classrooms
        System.out.println();
        Classroom2[] classrooms = timetable.getClasses();
        int classIndex = 1;
        for (Classroom2 bestClassroom : classrooms) {
            System.out.println("Classroom " + classIndex + ":");
            System.out.println(timetable.getModule(bestClassroom.getModuleId()).getDescription());
            System.out.println(timetable.getGroup(bestClassroom.getGroupId()).getName());
            System.out.println(timetable.getRoom(bestClassroom.getRoomId()).getRoomNumber());
            System.out.println(timetable.getProfessor(bestClassroom.getProfessorId()).getName());
            System.out.println(timetable.getTimeslot(bestClassroom.getTimeslotId()).getTimeslot());
            System.out.println("-----");
            classIndex++;
        }
        return classrooms;
    }
}