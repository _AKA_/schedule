package schedule.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import schedule.models.Room;

import java.util.List;

@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {

}