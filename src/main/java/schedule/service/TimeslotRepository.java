package schedule.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import schedule.models.Room;
import schedule.models.Timeslot;

import java.util.List;

@Repository
public interface TimeslotRepository extends CrudRepository<Timeslot, Long> {
}