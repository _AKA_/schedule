package schedule.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import schedule.models.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ScheduleService {


    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private ProfessorRepository professorRepository;
    @Autowired
    private TimeslotRepository timeslotRepository;
    @Autowired
    private ModuleRepository moduleRepository;

    public List<Room> getRooms() {
        return StreamSupport.stream(roomRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<Module> getModules() {
        return StreamSupport.stream(moduleRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<Professor> getProfessors() {
        return StreamSupport.stream(professorRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<Group> getGroups() {
        return StreamSupport.stream(groupRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<Timeslot> getTimeslots() {
        return StreamSupport.stream(timeslotRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }
}
