package schedule.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import schedule.models.Group;
import schedule.models.Module;
import schedule.models.Room;

import java.util.List;

@Repository
public interface ModuleRepository extends CrudRepository<Module, Long> {
}