package schedule.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import schedule.models.Module;
import schedule.models.Professor;
import schedule.models.Room;

import java.util.List;

@Repository
public interface ProfessorRepository extends CrudRepository<Professor, Long> {

}