package schedule;

import schedule.models.*;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Timetable {
    private HashMap<Integer, Room> rooms;
    private HashMap<Integer, Professor> professors;
    private HashMap<Integer, Module> modules;
    private HashMap<Integer, Group> groups;
    private HashMap<Integer, Timeslot> timeslots;
    private Classroom2 classes[];
    private int numClasses = 0;


    public Timetable(List<Room> rooms, List<Professor> professors, List<Module> modules, List<Group> groups, List<Timeslot> timeslots) {
        this.rooms = new HashMap<>();
        rooms.stream().forEach(item -> this.rooms.put(item.getId(), item));

        this.professors = new HashMap<>();
        professors.stream().forEach(item -> this.professors.put(item.getId(), item));

        this.modules = new HashMap<>();
        modules.stream().forEach(item -> this.modules.put(item.getId(), item));

        this.groups = new HashMap<>();
        groups.stream().forEach(item -> this.groups.put((int) item.getId(), item));

        this.timeslots = new HashMap<>();
        timeslots.stream().forEach(item -> this.timeslots.put(item.getId(), item));
    }

    public Timetable(Timetable cloneable) {
        this.rooms = cloneable.getRooms();
        this.professors = cloneable.getProfessors();
        this.modules = cloneable.getModules();
        this.groups = cloneable.getGroups();
        this.timeslots = cloneable.getTimeslots();
    }

    private HashMap<Integer, Group> getGroups() {
        return this.groups;
    }

    private HashMap<Integer, Timeslot> getTimeslots() {
        return this.timeslots;
    }

    private HashMap<Integer, Module> getModules() {
        return this.modules;
    }

    private HashMap<Integer, Professor> getProfessors() {
        return this.professors;
    }


    public void createClasses(Individual individual) {
        final Classroom2 classes[] = new Classroom2[this.getNumClasses()];
        // Get individual's chromosome
        int chromosome[] = individual.getChromosome();
        int chromosomePos = 0;
        int classIndex = 0;
        for (Group group : this.getGroupsAsArray()) {

            List<Integer> moduleIds = group.getModules().stream().map(item -> item.getId()).collect(Collectors.toList());
            for (Integer moduleId : moduleIds) {
                classes[classIndex] = new Classroom2(classIndex, (int) group.getId(), moduleId);
                // Add timeslot
                classes[classIndex].addTimeslot(chromosome[chromosomePos]);
                chromosomePos++;
                // Add room
                classes[classIndex].setRoomId(chromosome[chromosomePos]);
                chromosomePos++;

                // Add professor
                classes[classIndex].addProfessor(chromosome[chromosomePos]);
                chromosomePos++;
                classIndex++;
            }
            this.classes = classes;
        }
    }

    public Room getRoom(int roomId) {
        if (!this.rooms.containsKey(roomId)) {
            System.out.println("Rooms doesn't contain key " + roomId);
        }
        return (Room) this.rooms.get(roomId);
    }

    public HashMap<Integer, Room> getRooms() {
        return this.rooms;
    }

    public Room getRandomRoom() {
        Object[] roomsArray = this.rooms.values().toArray();
        Room room = (Room) roomsArray[(int) (roomsArray.length * Math.random())];
        return room;
    }

    public Professor getProfessor(int professorId) {
        return (Professor) this.professors.get(professorId);
    }

    public Module getModule(int moduleId) {
        return (Module) this.modules.get(moduleId);
    }

    public Group getGroup(int groupId) {
        return (Group) this.groups.get(groupId);
    }

    public Group[] getGroupsAsArray() {
        return (Group[]) this.groups.values().toArray(new
                Group[this.groups.size()]);
    }

    public Timeslot getTimeslot(int timeslotId) {
        return (Timeslot) this.timeslots.get(timeslotId);
    }

    public Timeslot getRandomTimeslot() {
        Object[] timeslotArray = this.timeslots.values().toArray();
        Timeslot timeslot = (Timeslot) timeslotArray[(int) (timeslotArray.length *
                Math.random())];
        return timeslot;
    }

    public Classroom2[] getClasses() {
        return this.classes;
    }

    public int getNumClasses() {
        if (this.numClasses > 0) {
            return this.numClasses;
        }
        int numClasses = 0;
        Group groups[] = (Group[]) this.groups.values().toArray(new
                Group[this.groups.size()]);
        for (Group group : groups) {
            numClasses += group.getModules().size();
        }
        this.numClasses = numClasses;
        return this.numClasses;
    }

    public int calcClashes() {
        int clashes = 0;
        for (Classroom2 classA : this.classes) {
            // Check room capacity
            int roomCapacity = this.getRoom(classA.getRoomId()).getCapacity();
            int groupSize = this.getGroup(classA.getGroupId()).getSize();

            if (roomCapacity < groupSize) {
                clashes++;
            }
            // Check if room is taken
            for (Classroom2 classB : this.classes) {
                if (classA.getRoomId() == classB.getRoomId() &&
                        classA.getTimeslotId() == classB.getTimeslotId()
                        && classA.getClassId() != classB.getClassId()) {
                    clashes++;
                    break;
                }
            }
            // Check if professor is available
            for (Classroom2 classB : this.classes) {
                if (classA.getProfessorId() == classB.getProfessorId() &&
                        classA.getTimeslotId() == classB.getTimeslotId()
                        && classA.getClassId() != classB.getClassId()) {
                    clashes++;
                    break;
                }
            }
        }
        return clashes;
    }
}