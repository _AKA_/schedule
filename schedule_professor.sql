INSERT INTO schedule.professor (id, name) VALUES (1, 'Zuev');
INSERT INTO schedule.professor (id, name) VALUES (2, 'Romanov');
INSERT INTO schedule.professor (id, name) VALUES (3, 'Besnosyk');
INSERT INTO schedule.professor (id, name) VALUES (4, 'Kalita');
INSERT INTO schedule.professor (id, name) VALUES (5, 'Petrenko');
INSERT INTO schedule.professor (id, name) VALUES (6, 'Sergeev-Gorchynsky');
INSERT INTO schedule.professor (id, name) VALUES (7, 'Kapshuk');
INSERT INTO schedule.professor (id, name) VALUES (8, 'Golubova');
INSERT INTO schedule.professor (id, name) VALUES (9, 'Dukhanina');
INSERT INTO schedule.professor (id, name) VALUES (10, 'Stus');
INSERT INTO schedule.professor (id, name) VALUES (11, 'Kirusha');
INSERT INTO schedule.professor (id, name) VALUES (12, 'Giorgizova');
INSERT INTO schedule.professor (id, name) VALUES (13, 'Minarchenko');
INSERT INTO schedule.professor (id, name) VALUES (14, 'Bokhonov');
INSERT INTO schedule.professor (id, name) VALUES (15, 'Stikanov');
INSERT INTO schedule.professor (id, name) VALUES (16, 'Drabko');
INSERT INTO schedule.professor (id, name) VALUES (17, 'Lyashenko');
INSERT INTO schedule.professor (id, name) VALUES (18, 'Gaidenko');
INSERT INTO schedule.professor (id, name) VALUES (19, 'Bulakh');
INSERT INTO schedule.professor (id, name) VALUES (20, 'Artuhov');
INSERT INTO schedule.professor (id, name) VALUES (21, 'Britov');